<?php

session_start();

class UserValidator {

	private $user;

	function __construct() {
		if (!class_exists('user')) {
			require __DIR__ . "/../model/user.php";
		}
		$this->user = new User;
	}

	public function isFieldEmpty($input) {
		if (!isset($input) || empty($input)) {
			return ('Field is empty !');
		}
		return (NULL);
	}

	public function isFormEmpty($form) {
		foreach ($form as $key => $value) {
			if (empty($value)) {
				return ("All fields must be completed");
			}
		}
		return (NULL);
	}

	public function validateEmail($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return ("Invalid email address.");
		}
		try {
			$dbEmail = $this->user->getEmail($email);
			if ($dbEmail) {
				return ("This email is already used.");
			}
		} catch (Exception $err) {
			die("Error : " . $err->getMessage() );
		}
		return (NULL);
	}

	public function validateUsername($username) {
		if (strlen($username) > 50 || !preg_match("~^[a-zA-Z0-9]*$~", $username)) {
			return ("Invalid username: max 50 alphanumeric characters.");
		}
		try {
			$dbUsername = $this->user->getUsername($username);
			if ($dbUsername) {
				return ("This username already exists.");
			}
		} catch (Exception $err) {
			die("Error : " . $err->getMessage() );
		}
		return (NULL);
	}

	public function validatePasswordEntries($password, $pwdRepeat) {
		if ($password != $pwdRepeat) {
			return ("Passwords do not match");
		}
		if (!preg_match("~^(?=.*[a-z|A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{12,72})~", $password)) {
			return ("Invalid password: must be between 12 and 72 characters, including letters, numbers and special characters.");
		}
		return (NULL);
	}

}
