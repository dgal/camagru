<?php

session_start();

class PictureValidator {

  public function checkFilterSettings($fields) {
    $check = TRUE;
		foreach ($fields as $field => $value) {
			if (!isset($value)) {
				$check = FALSE;
        break;
			}
		}
    if ($check == TRUE) {
      $required = ['dWidth', 'dHeight', 'dx', 'dy'];
      if (!$this->array_keys_exists($required, $fields)) {
        $check = FALSE;
      }
    }
		return ($check);
	}

  private function array_keys_exists(array $required, array $fields) {
    return (!array_diff_key(array_flip($required), $fields));
  }

  public function isFormatValid($uri) {
    $errFormat = NULL;
    if (strpos($uri, "png") === FALSE && strpos($uri, "jpg") === FALSE && strpos($uri, "jpeg") === FALSE) {
      $errFormat = 'Error file format: your picture format is invalid. Only .png, .jpg and .jpeg. are allowed.';
    }
    return ($errFormat);
  }

  public function isSizeValid($picture) {
	$errSize = NULL;
    $sizeMaxBytes = 2 * 1000000;
    if (strlen($picture) > $sizeMaxBytes) {
      $errSize = 'Error size: the image must be less than 2MB.';
    }
    return ($errSize);
  }

  public function isCommentValid($comment) {
    $errComment = NULL;
    if (empty($comment)) {
      $errComment = "Comment is empty.";
    } elseif (strlen($comment) > 255) {
      $errComment = "Comments are limited to 255 characters. Be concise !";
    }
    return ($errComment);
  }
}
