<?php

require "database.php";

/* CONNECT TO MYSQL */

$DB_ROOT_USER = 'root';
$DB_ROOT_PASSWORD = 'Camagru2021!';

try {
	$DBConnect = new PDO("mysql:host=localhost", $DB_ROOT_USER, $DB_ROOT_PASSWORD);
	$DBConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $err) {
	die("Connection to mysql failed with the following error: " . $err->getMessage() );
}

/* CREATE DATABASE */

$dbCreation = "CREATE DATABASE IF NOT EXISTS " . $DB_NAME;

try {
	$DBConnect->exec($dbCreation);
}
catch(PDOException $err) {
	die("Database creation failed with the following error: " . $err->getMessage() );
}
echo($DB_NAME . " was created successfully !\n");

/* CREATE USER */

$userCreation = "CREATE USER IF NOT EXISTS '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASSWORD'; GRANT ALL PRIVILEGES ON $DB_NAME . * TO '$DB_USER'@'localhost'";

try {
	$DBConnect->exec($userCreation);
}
catch(PDOException $err) {
	die("User creation failed with the following error: " . $err->getMessage() );
}
echo($DB_USER . " was created successfully !\n");

/* PREPARE QUERIES FOR TABLES */

$dbUsersTable = "CREATE TABLE IF NOT EXISTS `users` (
		UserID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
		Username VARCHAR(50) NOT NULL UNIQUE,
		Password VARCHAR(255) NOT NULL,
		Email VARCHAR(50) NOT NULL UNIQUE,
		TmpEmail VARCHAR(50) UNIQUE,
		Token VARCHAR(32) NOT NULL,
		Active INT NOT NULL DEFAULT 0,
		CommentNotif INT NOT NULL DEFAULT 1
		)";

$dbPicturesTable = "CREATE TABLE IF NOT EXISTS `pictures` (
		PictureId INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
		UserID INT NOT NULL,
		Img LONGBLOB,
		CreatedAt DATETIME DEFAULT CURRENT_TIMESTAMP
		)";

$dbCommentsTable = "CREATE TABLE IF NOT EXISTS `comments` (
		UserID INT NOT NULL,
		PictureId INT NOT NULL,
		Content VARCHAR(255),
		CreatedAt DATETIME DEFAULT CURRENT_TIMESTAMP
		)";

$dbLikesTable = "CREATE TABLE IF NOT EXISTS `likes` (
		PictureId INT NOT NULL,
		UserID INT NOT NULL
		)";


/* CONNECT TO MYSQL WITH USER */

try {
	$DB = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
	$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $err) {
	die("Connection to mysql by user failed with the following error: " . $err->getMessage() );
}

/* CREATE TABLES */

try {
	$DB->exec($dbUsersTable);
}
catch (PDOException $err) {
	die("Creation of the `users` table failed with the following error: " . $err->getMessage() );
}

try {
	$DB->exec($dbPicturesTable);
}
catch (PDOException $err) {
	die("Creation of the `pictures` table failed with the following error: " . $err->getMessage() );
}

try {
	$DB->exec($dbCommentsTable);
}
catch (PDOException $err) {
	die("Creation of the `comments` table failed with the following error: " . $err->getMessage() );
}

try {
	$DB->exec($dbLikesTable);
}
catch (PDOException $err) {
	die("Creation of the `likes` table failed with the following error: " . $err->getMessage() );
}

echo ("All tables were created successfully !");
