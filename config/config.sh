#!/bin/bash

# CHECK IF ROOT MODE
if (($EUID != 0))
then echo "Please run as root"
	exit 1
fi

# UPDATES
apt update && apt upgrade -y

# INSTALL PACKAGES
apt install apache2 -y
apt install php -y
apt install msmtp -y
apt install msmtp-mta -y
apt install php-imagick -y

# PERMISSION UFW
ufw allow 'Apache Full'

# COPY CONFIG FILES
cp config/conf_files/msmtprc /etc/.
cp config/conf_files/camagru.conf /etc/apache2/sites-available/.
cp config/conf_files/camagru-ssl.conf /etc/apache2/sites-available/.
cp config/conf_files/ssl-params.conf /etc/apache2/conf-available/.

# CONFIGURE VIRTUAL HOSTS
mkdir /var/www/camagru
a2ensite camagru.conf
a2dissite 000-default.conf
a2enmod ssl
a2enmod headers
a2ensite camagru-ssl
a2enmod rewrite

# CREATE SSL CERTIFICATE
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt

# UPDATE php.ini FILE
sed -i 's|;sendmail_path =|sendmail_path = "/usr/bin/msmtp -t"|' /etc/php/7.2/apache2/php.ini
sed -i 's|error_reporting|;error_reporting|' /etc/php/7.2/apache2/php.ini
echo "extension=imagick" >> /etc/php/7.2/apache2/php.ini

# ALLOW ROOT IN MYSQL
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Camagru2021!'" | sudo mysql

# SET NEW LINE IN HOSTS FILE
echo "127.0.0.1	www.camagru.com" >> /etc/hosts

# COPY PROJECT REPO
cp -R * /var/www/camagru/.
cp .htaccess /var/www/camagru/.

#CHANGE RIGHTS ON SERVER
sudo chown -R www-data:www-data /var/www/camagru
chmod -R 755 /var/www/camagru

# STOP USELESS SERVICES
systemctl stop nginx
update-rc.d -f nginx disable

# START APACHE
systemctl start apache2

# EXECUTE DATABASE SETUP
php -f /var/www/camagru/config/setup.php
