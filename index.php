<?php

session_start();

$request = strtok($_SERVER['REQUEST_URI'], '?');

$controllerName = null;
$controllerAction = null;

switch ($request) {
  case '/':
	case '':
		$controllerName = 'home';
		$controllerAction = 'index';
		break;
	case '/register':
		$controllerName = 'user';
		$controllerAction = 'registerForm';
		break;
	case '/register-submit':
		$controllerName = 'user';
		$controllerAction = 'registerSubmit';
		break;
	case '/login':
		$controllerName = 'user';
		$controllerAction = 'login';
		break;
	case '/logout':
		$controllerName = 'user';
		$controllerAction = 'logout';
		break;
	case '/validate-account':
		$controllerName = 'user';
		$controllerAction = 'validateAccount';
		break;
  case '/forgotten-password':
		$controllerName = 'user';
  	$controllerAction = 'forgottenPasswordForm';
		break;
  case '/forgotten-password-submit':
  	$controllerName = 'user';
    $controllerAction = 'forgottenPasswordSubmit';
    break;
  case '/reset-password':
    $controllerName = 'user';
    $controllerAction = 'resetPasswordForm';
    break;
  case '/reset-password-submit':
    $controllerName = 'user';
    $controllerAction = 'resetPasswordSubmit';
    break;
  case '/settings':
    $controllerName = 'user';
    $controllerAction = 'displaySettings';
    break;
  case '/change-settings':
    $controllerName = 'user';
    $controllerAction = 'changeSettings';
    break;
  case '/confirm-email':
    $controllerName = 'user';
    $controllerAction = 'confirmEmail';
    break;
  case '/delete-account-form':
    $controllerName = 'user';
    $controllerAction = 'deleteAccountForm';
    break;
  case '/camera':
    $controllerName = 'pictures';
    $controllerAction = 'displayCamera';
    break;
  case '/create-montage':
    $controllerName = 'pictures';
    $controllerAction = 'createMontage';
    break;
  case '/add-like':
    $controllerName = 'pictures';
    $controllerAction = 'addLike';
    break;
  case '/my-pictures':
    $controllerName = 'pictures';
    $controllerAction = 'displayUserPictures';
    break;
  case '/picture-focus':
    $controllerName = 'pictures';
    $controllerAction = 'displayPicture';
    break;
  case '/comment-submit':
    $controllerName = 'pictures';
    $controllerAction = 'addCommentForm';
    break;
  case '/delete-picture-request':
    $controllerName = 'pictures';
    $controllerAction = 'deletePictureRequest';
    break;
  case '/delete-picture-form':
    $controllerName = 'pictures';
    $controllerAction = 'deletePictureForm';
    break;
	default:
		http_response_code(404);
		echo "NOT FOUND";
		break;
}

if ($controllerName !== null && $controllerAction !== null) {
	require sprintf('controller/%s.php', $controllerName);
	$controllerClassName = sprintf('%sController', ucfirst($controllerName));
	$controller = new $controllerClassName;
	if ($controllerAction !== null) {
	    $controllerAction = sprintf('%sAction', $controllerAction);
		$controller->$controllerAction();
	}
}
exit;
