<html>
 <body>
 <p>Hi <?php echo $username?>,</p>
	<p>We received your request to reset your password, please click on the link below !</p>
	<a href="<?php echo $resetLink ?>">Reset password</a>
  <p>If you did not ask for a new password, just ignore this email.</p>
	<br>
	<p>The Camagru Team</p>
	</body>
</html>
