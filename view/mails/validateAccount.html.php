<html>
 <body>
 <p>Hi <?php echo $username?>,</p>
	<p>Welcome to Camagru ! To complete your registration, please confirm your email address below:</p>
	<a href="<?php echo $confirmationLink ?>&action=confirm">Confirm Here !</a>
	<br>
	<p>The Camagru Team</p>
	<br>
	<p>Did you receive this email without signing up ? <a href="<?php echo $confirmationLink ?>&action=delete">Click here</a> to delete your account.</p>
	</body>
</html>
