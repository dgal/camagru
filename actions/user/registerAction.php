<?php

require_once __DIR__ . "/../action.php";

class RegisterAction extends Action {

	private $errorBag = array();
	private $userValidator;
	private $user;
	private $tokenSubAction;

	function __construct() {
		if (!class_exists('userValidator')) {
			require __DIR__ . "/../../validators/userValidator.php";
		}
		$this->userValidator = new UserValidator;
		$this->user = $this->createModelInstance('user');
		$this->tokenSubAction = $this->createSubActionInstance('tokenSubAction');
	}

	public function checkRegistrationForm($registrationForm) {
		if ($errEmpty = $this->userValidator->isFormEmpty($registrationForm) != NULL) {
			$this->errorBag['errEmpty'] = $errEmpty;
		} else {
			if (($errEmail = $this->userValidator->validateEmail($registrationForm['email'])) != NULL) {
				$this->errorBag['errEmail'] = $errEmail;
			}
			if (($errUsername = $this->userValidator->validateUsername($registrationForm['username'])) != NULL) {
				$this->errorBag['errUsername'] = $errUsername;
			}
			if (($errPassword = $this->userValidator->validatePasswordEntries($registrationForm['pwd'], $registrationForm['pwdRepeat'])) != NULL) {
				$this->errorBag['errPassword'] = $errPassword;
			}
		}
		return ($this->errorBag);
	}

	public function addUserToDatabase($registrationForm) {
		$pwdHashed = $this->hashPassword($registrationForm['pwd']);
		$token = $this->tokenSubAction->generateToken($registrationForm['username']);
		$tokenHashed = $this->tokenSubAction->hashToken($token);
		$registrationForm['username'] = ucfirst($registrationForm['username']);
		if ($pwdHashed && $tokenHashed) {
			try {
				$this->user->createUser($registrationForm, $pwdHashed, $tokenHashed);
			} catch (Exception $err) {
				die("Error: " . $err->getMessage() );
			}
			return ($token);
		}
		return (NULL);
	}

	private function hashPassword($password) {
		return (password_hash($password, PASSWORD_BCRYPT));
	}

	public function checkQueryStringValues($username, $token) {
		if ($this->checkUsername($username) && $this->tokenSubAction->checkToken("Username", $username, $token)) {
			return (TRUE);
		}
		return (FALSE);
	}

	private function checkUsername($username) {
		try {
			$dbUsername = $this->user->getUsername($username);
		} catch (Exception $err) {
			die("Error: " . $err->getMessage() );
		}
		if ($dbUsername) {
			return (TRUE);
		}
		return (FALSE);
	}

	public function updateUserToken($username) {
		return ($this->tokenSubAction->updateToken($username, "Username"));
	}

	public function activateAccount($username) {
		try {
			$this->user->updateUserActive($username, TRUE);
		} catch (Exception $err) {
			die ("Error: " . $err->getMessage() );
		}
	}

}
