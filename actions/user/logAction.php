<?php

require_once __DIR__ . "/../action.php";

class LogAction extends Action {

	private $error = NULL;
	private $user;
	private $tokenSubAction;
	private $passwordSubAction;

	function __construct() {
		$this->user = $this->createModelInstance('user');
		$this->tokenSubAction = $this->createSubActionInstance('tokenSubAction');
		$this->passwordSubAction = $this->createSubActionInstance('passwordSubAction');
	}

	public function checkLoginForm($username, $password) {
		$log = FALSE;
		if (!empty($username) && !empty($password)) {
			try {
				$dbUsername = $this->user->getUsername($username);
				if ($dbUsername && $this->passwordSubAction->verifyPassword($password, 'Username', $dbUsername) == TRUE) {
						$log = TRUE;
				}
			} catch (Exception $err) {
				die('Error : ' . $err->getMessage() );
			}
		}
		if ($log == FALSE) {
			$this->error = 'Invalid username and / or password.';
		}
		return ($this->error);
	}

	public function isUserActive($username) {
		return ($this->user->getActive($username));
	}

	public function setLoginSession($username) {
		try {
			$userRow = $this->user->getUserInfo($username);
			$_SESSION['userId'] = $userRow['UserID'];
			$_SESSION['username'] = $userRow['Username'];
			$_SESSION['loggedin'] = TRUE;
		} catch (Exception $err) {
			die('Error : ' . $err->getMessage() );
		}
	}

	public function getEmailFromDatabase($username) {
		try {
			$dbEmail = $this->user->getEmailByUsername($username);
		} catch (Exception $err) {
			die('Error : ' . $err->getMessage() );
		}
		return ($dbEmail);
	}

	public function updateUserToken($username) {
		return ($this->tokenSubAction->updateToken($username, "Username"));
	}

	public function logout() {
		if(isset($_POST["logout"])) {
			$this->clearSession();
		}
	}

	public function clearSession() {
			$_SESSION['loggedin'] = false;
			$_SESSION = array();
	}
	
}
