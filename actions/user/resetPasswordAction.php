<?php

require_once __DIR__ . "/../action.php";

class ResetPasswordAction extends Action {

  private $emailSubAction;
  private $user;
  private $tokenSubAction;
  private $userValidator;
  private $passwordSubAction;

  function __construct() {
    if (!class_exists('userValidator')) {
      require __DIR__ . "/../../validators/userValidator.php";
    }
    $this->userValidator = new UserValidator;
    $this->emailSubAction = $this->createSubActionInstance('emailSubAction');
    $this->tokenSubAction = $this->createSubActionInstance('tokenSubAction');
    $this->passwordSubAction = $this->createSubActionInstance('passwordSubAction');
    $this->user = $this->createModelInstance('user');
  }

  public function checkEmail($email) {
    $errEmail = $this->emailSubAction->checkEmailEntry($email);
    return ($errEmail);
  }

  public function checkIfUserExists($email) {
    $username = NULL;
    try {
      if ($dbEmail = $this->user->getEmail($email)) {
        $username = $this->user->getUsernameByEmail($dbEmail);
      }
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
    return ($username);
  }

  public function updateUserToken($username) {
    return ($this->tokenSubAction->updateToken($username, "Username"));
  }

  public function resetPassword($password, $pwdRepeat, $username) {
    $errPassword = $this->checkPassword($password, $pwdRepeat);
    if (empty($errPassword)) {
      $this->passwordSubAction->updateUserPassword($password, "Username", $username);
    }
    return ($errPassword);
  }

  public function checkPassword($password, $pwdRepeat) {
    if (empty($password) || empty($pwdRepeat)) {
      $errPassword = "Error: both fields must be completed !";
    } else {
      $errPassword = $this->userValidator->validatePasswordEntries($password, $pwdRepeat);
    }
    return ($errPassword);
  }

}
