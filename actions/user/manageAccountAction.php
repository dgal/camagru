<?php

require_once __DIR__ . "/../action.php";

class ManageAccountAction extends Action {

  private $userValidator;
  private $user;
  private $passwordSubAction;
  private $tokenSubAction;
  private $error = NULL;

  function __construct() {
    $this->user = $this->createModelInstance('user');
    $this->passwordSubAction = $this->createSubActionInstance('passwordSubAction');
    $this->tokenSubAction = $this->createSubActionInstance('tokenSubAction');

    if (!class_exists('userValidator')) {
      require __DIR__ . "/../../validators/userValidator.php";
    }
    $this->userValidator = new UserValidator;
  }

  public function getCheckboxStatus($userId) {
    $checkStatus = '';
    try {
      $dbCommentNotif = $this->user->getCommentNotif($userId);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
    if ($dbCommentNotif == TRUE) {
      $checkStatus = 'checked';
    }
    return ($checkStatus);
  }

  public function changeUsername($userId) {
    $input = '$_POST["username"]';
    $errUsername = $this->userValidator->isFieldEmpty($input);
    if (empty($errUsername)) {
      $username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
      $errUsername = $this->userValidator->validateUsername($username);
    }
    if (empty($errUsername)) {
      try {
        $this->user->updateUsername(ucfirst($username), $userId);
      } catch (Exception $err) {
        die("Error: " . $err->getMessage() );
      }
    }
    return ($errUsername);
  }

  public function changeEmail($userId) {
    $input = '$_POST["email"]';
    $errEmail = $this->userValidator->isFieldEmpty($input);
    if (empty($errEmail)) {
      $email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
      $errEmail = $this->userValidator->validateEmail($email);
    }
    if (empty($errEmail)) {
      try {
        $this->user->UpdateUserTmpEmail($email, $userId);
      } catch (Exception $err) {
        die("Error: " . $err->getMessage() );
      }
    }
    return ($errEmail);
  }

  public function checkQueryStringValues($userId, $token) {
    if ($this->checkUserId($userId) && $this->tokenSubAction->checkToken("UserID", $userId, $token)) {
      return (TRUE);
    }
    return (FALSE);
  }

  private function checkUserId($userId) {
    try {
      $dbId = $this->user->checkIfUserIdExists($userId);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
    return ($dbId);
  }

  public function updateUserToken($userId) {
    return ($this->tokenSubAction->updateToken($userId, "UserID"));
  }

  public function updateUserEmail($userId) {
    try {
      $newEmail = $this->user->getTmpEmailByUserId($userId);
      $this->user->updateUserEmail($newEmail, $userId);
      $this->user->updateUserTmpEmail('', $userId);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
  }

  public function changePassword($userId) {
    $errPassword = $this->checkPwdFields();
    if (empty($errPassword)) {
      $oldPwd = htmlspecialchars($_POST['oldPwd'], ENT_QUOTES, 'UTF-8');
      $newPwd = htmlspecialchars($_POST['pwd'], ENT_QUOTES, 'UTF-8');
      $newPwdRepeat = htmlspecialchars($_POST['pwdRepeat'], ENT_QUOTES, 'UTF-8');

      $errPassword = $this->checkPasswords($userId, $oldPwd, $newPwd, $newPwdRepeat);
    }
    if (empty($errPassword)) {
      $this->passwordSubAction->updateUserPassword($newPwd, "UserID", $userId);
    }
    return ($errPassword);
  }

  private function checkPasswords($userId, $oldPwd, $newPwd, $newPwdRepeat) {
    $errPwd = NULL;
    if ($this->passwordSubAction->verifyPassword($oldPwd, "UserID", $userId) == FALSE) {
      $errPwd = "Incorrect old password !";
    }
    if (empty($errPwd)) {
      $errPwd = $this->userValidator->validatePasswordEntries($newPwd, $newPwdRepeat);
    }
    return ($errPwd);
  }

  private function checkPwdFields() {
    $errEmpty = NULL;
    if (!isset($_POST['oldPwd']) || !isset($_POST['pwd']) || !isset($_POST['pwdRepeat'])
    || empty($_POST['oldPwd']) || empty($_POST['pwd']) || empty($_POST['pwdRepeat'])) {
      $errEmpty = 'All fields must be completed !';
    }
    return ($errEmpty);
  }

  public function changeCommentNotif($userId) {
    $value = 1;
    if (!isset($_POST['commentNotif'])) {
      $value = 0;
    }
    try {
      $this->user->updateUserCommentNotif($value, $userId);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
  }

  public function deleteAccountRequest($userId) {
    $errDelete = NULL;
    if (!isset($_POST['pwd']) || empty($_POST['pwd'])) {
      $errDelete = 'You must enter your password in order to delete your account.';
    } else {
      $password = htmlspecialchars($_POST['pwd'], ENT_QUOTES, 'UTF-8');
      if ($this->passwordSubAction->verifyPassword($password, "UserID", $userId) == FALSE) {
        $errDelete = 'Incorrect password !';
      }
    }
    return ($errDelete);
  }

  public function deleteAccount($column, $user) {
    try {
      $this->user->deleteUser($column, $user);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
  }

}
