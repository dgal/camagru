<?php

require_once __DIR__ . "/../action.php";

class LikesAction extends Action {

  private $likesModel;

  function __construct() {
    $this->likesModel = $this->createModelInstance('likesModel');
  }

  public function countLikes($pictureId) {
    try {
      $likes = $this->likesModel->countLikes($pictureId);
    } catch (Exception $err) {
      die("Error (code APL): " . $err->getMessage() );
    }
    return ($likes);
  }

  public function isPictureLiked($pictureId) {
    $like = FALSE;
    if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
      try {
        $like = $this->likesModel->isLiked($_SESSION['userId'], $pictureId);
      } catch (Exception $err) {
        die("Error (code APL): " . $err->getMessage() );
      }
    }
    return ($like);
  }

  public function addLike($pictureId) {
    if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
      try {
        if ($this->isPictureLiked($pictureId) == FALSE) {
          $this->likesModel->addLike($_SESSION['userId'], $pictureId);
        } else {
          $this->likesModel->deleteUserLike($_SESSION['userId'], $pictureId);
        }
      } catch (Exception $err) {
        die("Error (code APL): " . $err->getMessage() );
      }
    }
  }

  public function deleteLikes($column, $value) {
    try {
      $this->likesModel->deleteAll($column, $value);
    } catch (Exception $err) {
      die("Error (code APL): " . $err->getMessage() );
    }
  }

}
