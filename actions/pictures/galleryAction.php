<?php

require_once __DIR__ . "/../action.php";

class GalleryAction extends Action {

  private $picturesModel;
  private $user;

  function __construct() {
    $this->picturesModel = $this->createModelInstance('picturesModel');
    $this->user = $this->createModelInstance('user');
  }

  public function getPageNumber() {
    $page = 0;
    if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] >= 0) {
      $page = $_GET['page'];
    }
    return ((int)$page);
  }

  public function getPictures($page, $userId) {
    $maxPictures = 12;
    try {
      if ($userId === FALSE) {
        $pictureList = $this->picturesModel->selectAllPictures($page, $maxPictures);
      } else {
        $pictureList = $this->picturesModel->selectUserPictures($page, $maxPictures, $userId);
      }
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
    return ($pictureList);
  }

  public function getUsername($userId) {
    try {
      $username = $this->user->getValueBy('Username', 'UserID', $userId);
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
    return ($username);
  }

  public function getPageMax($userId) {
    try {
      if ($userId === FALSE) {
        $count = $this->picturesModel->countAllPictures();
      } else {
        $count = $this->picturesModel->countUserPictures($userId);
      }
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
    $maxPictures = 12;
    if ($count == $maxPictures) {
      $count -= 1;
    }
    $pageMax = $count / $maxPictures;
    return ($pageMax);
  }

  public function getPicture($pictureId) {
    try {
      $picture = $this->picturesModel->selectPicture($pictureId);
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
    return ($picture);
  }

  public function checkPictureRights($pictureId, $userId) {
    try {
      $output = $this->picturesModel->getPictureByUserId($pictureId, $userId);
      if ($output) {
        return (TRUE);
      }
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
    return(FALSE);
  }

  public function deletePicture($pictureId) {
    try {
      $this->picturesModel->deletePicture($pictureId);
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
  }

  public function deleteAllPictures($column, $user) {
    try {
      $this->picturesModel->deleteAll($column, $user);
    } catch (Exception $err) {
      die("Error (code APG): " . $err->getMessage() );
    }
  }
}
