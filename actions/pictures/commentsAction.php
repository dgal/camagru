<?php

require_once __DIR__ . "/../action.php";

class CommentsAction extends Action {

  private $user;
  private $commentsModel;
  private $picturesModel;
  private $pictureValidator;

  function __construct() {
    $this->user = $this->createModelInstance('user');
    $this->commentsModel = $this->createModelInstance('commentsModel');
    $this->picturesModel = $this->createModelInstance('picturesModel');
    if (!class_exists('pictureValidator')) {
      require __DIR__ . "/../../validators/pictureValidator.php";
    }
    $this->pictureValidator = new PictureValidator;
  }

  public function countComments($pictureId) {
    try {
      $count = $this->commentsModel->countComments($pictureId);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return ($count);
  }

  public function getComments($pictureId) {
    try {
      $commentList = $this->commentsModel->selectAllComments($pictureId);
      if (!empty($commentList)) {
        foreach ($commentList as &$comment) {
          $comment['username'] = $this->user->getValueBy('Username', 'UserID', $comment['UserID']);
        }
      }
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return ($commentList);
  }

  private function getUsername($userId) {
    try {
      $username = $this->user->getValueBy('Username', 'UserID', $userId);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return ($username);
  }

  public function validateComment($comment) {
    $error = NULL;
    if (!isset($_SESSION['userId']) || empty($_SESSION['userId'])) {
      $error = "You need to log in to comment this picture.";
    }
    if ($error === NULL) {
        $error = $this->pictureValidator->isCommentValid($comment);
    }
    return ($error);
  }

  public function addComment($userId, $pictureId, $comment) {
    try {
      $this->commentsModel->insertComment($userId, $pictureId, $comment);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
  }

  private function getPictureAuthor($pictureId) {
    try {
      $userId = $this->picturesModel->getValueBy('UserID', 'PictureId', $pictureId);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return ($userId);
  }

  public function hasMailOption($pictureId) {
    try {
      $userId = $this->getPictureAuthor($pictureId);
      $option = $this->user->getCommentNotif($userId);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return ($option);
  }

  public function prepareEmailArgs($pictureId) {
    $userId = $this->getPictureAuthor($pictureId);
    try {
      $emailArgs = $this->user->getBothValuesBy('Email', 'Username', 'UserID', $userId);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
    return($emailArgs);
  }

  public function deleteComments($column, $value) {
    try {
      $this->commentsModel->deleteAll($column, $value);
    } catch (Exception $err) {
      die("Error (code APC): " . $err->getMessage() );
    }
  }
}
