<?php

require_once __DIR__ . "/../action.php";

class MontageAction extends Action {

  private $error;
  private $pictureValidator;
  private $decodedPicture;
  private $finalPicture;
  private $picturesModel;

  function __construct() {
    if (!class_exists('pictureValidator')) {
      require __DIR__ . "/../../validators/pictureValidator.php";
    }
    $this->pictureValidator = new PictureValidator;
    $this->picturesModel = $this->createModelInstance('picturesModel');
  }

  public function checkPostEntries() {
    if (!isset($_POST['rawPicture']) || !isset($_POST['filterPath']) || !isset($_POST['filterSettings'])
        || empty($_POST['rawPicture']) || empty($_POST['filterPath']) || empty($_POST['filterSettings'])) {
          $this->error = 'Error Empty (code: APM): you must select a picture AND a filter !';
    }

    if (empty($this->error)) {
      $encodedPicture = htmlspecialchars($_POST['rawPicture'], ENT_QUOTES, 'UTF-8');
      $encodedPicture = explode(',', $encodedPicture);
      $this->error = $this->pictureValidator->isFormatValid($encodedPicture[0]);
    }

    if (empty($this->error)) {
      $encodedPicture[1] = str_replace(' ', '+', $encodedPicture[1]);
      $this->decodedPicture = $this->decodePicture($encodedPicture[1]);
      $this->error = $this->pictureValidator->isSizeValid($this->decodedPicture);
    }

    return($this->error);
  }

  private function decodePicture($encodedPicture) {
    return (base64_decode($encodedPicture));
  }

  public function createMontage() {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . htmlspecialchars($_POST['filterPath'], ENT_QUOTES, 'UTF-8');
    $filterSettings = json_decode($_POST['filterSettings'], true);
    if ($this->pictureValidator->checkFilterSettings($filterSettings) === FALSE) {
      return('An error has occurred (code: APM). Please try again later.');
    }

    $filter = new Imagick();
    $this->finalPicture = new Imagick();
    if ($filter->readImage($filePath) === FALSE || $this->finalPicture->readImageBlob($this->decodedPicture) === FALSE) {
        $this->error = 'Error Read (code: APM): image cannot be read';
    } else {
      $resize = $filter->scaleImage($filterSettings['dWidth'], $filterSettings['dHeight']);
      if ($this->finalPicture->compositeImage($filter, imagick::COMPOSITE_DEFAULT, $filterSettings['dx'], $filterSettings['dy']) === FALSE) {
        $this->error = 'Processing error (code: APM): cannot edit images';
      }
    }
    return ($this->error);
  }

  public function addPictureToDatabase() {
    $userId = $_SESSION['userId'];
    try {
      $this->picturesModel->insertNewPicture($userId, 'data:image/jpg;base64,' . base64_encode($this->finalPicture->getImageBlob()));
    } catch (Exception $err) {
      die("Error : " . $err->getMessage());
    }
  }

}
