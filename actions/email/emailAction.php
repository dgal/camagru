<?php

include __DIR__ . "/../../helper/mailTemplate.php";

class EmailAction {

	public function sendEmail($email, $user, $data, $methodName) {
		if (!$data) {
			throw new Exception("data variable is empty (code: AE).");
		}
		$header = "Content-type: text/html; charset=iso-8859-1";
		$mailTemplate = new MailTemplate;
		$message = $mailTemplate->$methodName($user, $data);
		$subject = $this->getSubject($methodName);
		if ((mail($email, $subject, $message, $header)) == FALSE) {
			throw new Exception("call to function mail() failed (code: AE).");
		}
	}

	private function getSubject($methodName) {
		$subject = NULL;
		if ($methodName == 'validateAccountMessage') {
			$subject = 'Camagru Account Validation';
		}
		if ($methodName == 'resetPasswordMessage') {
			$subject = 'Reset your password on Camagru';
		}
		if ($methodName == 'confirmEmailMessage') {
			$subject = 'Confirm your new Email address for Camagru';
		}
		if ($methodName == 'notifyComment') {
			$subject = 'You have a new comment on Camagru !';
		}
		return ($subject);
	}
}
