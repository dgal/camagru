<?php

class EmailSubAction {

  private $user;

  function __construct() {
    if (!class_exists('user')) {
      require __DIR__ . "/../../model/user.php";
    }
    $this->user = new User;
  }

  public function checkEmailEntry($email) {
    $errEmail = "Field must be completed !";
    if (!empty($email)) {
      $errEmail = $this->checkIfEmailExists($email);
    }
    return($errEmail);
  }

  private function checkIfEmailExists($email) {
    $errEmail = NULL;
    try {
      if (!$this->user->getEmail($email)) {
        $errEmail = "Email does not exist.";
      }
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
    return ($errEmail);
  }

}
