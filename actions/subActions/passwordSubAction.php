<?php

class PasswordSubAction {

  private $user;

  function __construct() {
    if (!class_exists('user')) {
      require __DIR__ . "/../../model/user.php";
    }
    $this->user = new user;
  }

  public function verifyPassword($password, $column, $value) {
    $dbPassword = $this->getDbPassword($column, $value);
    if ($dbPassword) {
      return (password_verify($password, $dbPassword));
    }
    return (FALSE);
  }

  private function getDbPassword($column, $value) {
    try {
      $dbPassword = $this->user->getPassword($column, $value);
    } catch (Exception $err) {
      die('Error: ' . $err->getMessage() );
    }
    return ($dbPassword);
  }

  public function updateUserPassword($password, $column, $value) {
    $pwdHashed = $this->hashPassword($password);
    try {
      $this->user->updateUserPassword($pwdHashed, $column, $value);
    } catch (Exception $err) {
      die("Error: " . $err->getMessage() );
    }
  }

  private function hashPassword($password) {
    return (password_hash($password, PASSWORD_BCRYPT));
  }

}
