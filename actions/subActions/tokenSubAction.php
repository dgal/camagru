<?php

class TokenSubAction {

  private $user;

  function __construct() {
		if (!class_exists('user')) {
			require __DIR__ . "/../../model/user.php";
		}
		$this->user = new user;
	}

  public function generateToken($user) {
    $randomId = $user . rand(0, 1000);
    $token = $this->hashToken($randomId);
    return ($token);
  }

  public function hashToken($token) {
    return (md5($token));
  }

  public function getDbToken($column, $user) {
    $dbToken = NULL;
    try {
			$dbToken = $this->user->getToken($column, $user);
		} catch (Exception $err) {
			die("Error: " . $err->getMessage() );
		}
    return ($dbToken);
  }

  public function checkToken($column, $user, $token) {
		$tokenHashed = $this->hashToken($token);
    $dbToken = $this->getDbToken($column, $user);
		return ($tokenHashed == $dbToken) ? TRUE : FALSE ;
	}

  public function updateToken($user, $column) {
    if (!empty($user) && !empty($column)) {
      try {
        $token = $this->generateToken($user);
        $tokenHashed = $this->hashToken($token);
        $this->user->updateUserToken($tokenHashed, $column, $user);
      } catch (Exception $err) {
        die("Error: " . $err->getMessage() );
      }
    }
    return ($token);
	}

}
