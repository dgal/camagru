<?php

session_start();

class Action {

  protected function createModelInstance($class) {
    if (!class_exists($class)) {
      require __DIR__ . "/../model/$class.php";
    }
    $class = ucfirst($class);
    return(new $class);
  }

  protected function createSubActionInstance($class) {
    if (!class_exists($class)) {
      require __DIR__ . "/subActions/$class.php";
    }
    $class = ucfirst($class);
    return(new $class);
  }
}
