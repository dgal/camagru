var settings = {
  "/public/images/filters/frame.png": { dWidth: 640, dHeight: 440, dx: -10, dy: 5, },
  "/public/images/filters/oops.png": { dWidth: 180, dHeight: 200, dx: 5, dy: 5, },
  "/public/images/filters/versus.png": { dWidth: 235, dHeight: 450, dx: 190, dy: 0, },
  "/public/images/filters/wow.png": { dWidth: 524, dHeight: 250, dx: 190, dy: -20, },
};

var width = 600;
var height = 0;
var streaming = false;
var video = null;
var canvas = null;
var startbutton = null;
var switchbutton = null;
var uploadFile = null;
var photoInput = null;
var imgCounter = 0;

window.addEventListener('load', startup, false);

function startup() {
  video = document.getElementById('video');
  canvas = document.getElementById('canvas');
  startbutton = document.getElementById('startbutton');
  switchbutton = document.getElementById('switchbutton');
  filterPath = '/public/images/filters/';
  defaultFilter = filterPath.concat('frame.png');
  uploadFile = document.getElementById('uploadFile');
  photoInput = document.getElementById('photoInput');

  navigator.mediaDevices.getUserMedia({video: true, audio: false})
  .then(function(stream) {
    video.srcObject = stream;
    video.play();
  })
  .catch(function(err) {
    console.log("An error has occurred: " + err);
  });

  switchbutton.addEventListener('click', function(ev) {
    if (switchbutton.checked == false ){
      video.style.display = 'block';
      photoInput.style.display = 'none';
      uploadFile.style.display = 'none';
      startbutton.removeAttribute('disabled', "");
    } else {
      photoInput.setAttribute('width', width);
      photoInput.setAttribute('height', height);
      video.style.display = 'none';
      photoInput.style.display = 'block';
      uploadFile.style.display = 'block';
      startbutton.setAttribute('disabled', "");
    }
  }, false);

  video.addEventListener('canplay', function(ev){
    if (!streaming) {
      height = video.videoHeight / (video.videoWidth / width);
      if (isNaN(height)) {
        height = width / (4/3);
      }
      video.setAttribute('width', width);
      video.setAttribute('height', height);
      streaming = true;
      setFilterPreview();
    }
  }, false);

  uploadFile.addEventListener("change", function(ev){
    ev.stopPropagation();
    ev.preventDefault();
    let reader = new FileReader();
    var file = uploadFile.files[0];
    reader.onload = function(){
      photoInput.src = reader.result;
      startbutton.removeAttribute('disabled', "");
    }
    reader.readAsDataURL(file);
  }, false);

  startbutton.addEventListener('click', function(ev){
    takepicture();
    ev.preventDefault();
  }, false);
}

function setFilterPreview() {
  var img = new Image();
  var context = canvasPreview.getContext('2d');
  img.width = settings[defaultFilter].dWidth;
  img.height = settings[defaultFilter].dHeight;
  img.onload = function() {
    canvasPreview.width = width;
    canvasPreview.height = height;
    context.clearRect(0, 0, canvasPreview.width, canvasPreview.height);
    context.drawImage(img, settings[defaultFilter].dx, settings[defaultFilter].dy, settings[defaultFilter].dWidth, settings[defaultFilter].dHeight);
  };
  img.src = defaultFilter;
}

function changeFilterPreview(event, filter) {
  event.preventDefault();
  event.stopPropagation();
  defaultFilter = filterPath.concat(filter);
  setFilterPreview();
}

function takepicture() {
  var context = canvas.getContext('2d');
  var filter = new Image();
  filter.src = defaultFilter;

  if (width && height) {
    canvas.width = width;
    canvas.height = height;
    if (switchbutton.checked == false ) {
      context.drawImage(video, 0, 0, width, height);
      var photoURL = canvas.toDataURL('image/png');
      context.drawImage(filter, settings[defaultFilter].dx, settings[defaultFilter].dy, settings[defaultFilter].dWidth, settings[defaultFilter].dHeight);
    } else {
      try{
        context.drawImage(photoInput, 0, 0, width, height);
        var photoURL = canvas.toDataURL('image/png');
        context.drawImage(filter, settings[defaultFilter].dx, settings[defaultFilter].dy, settings[defaultFilter].dWidth, settings[defaultFilter].dHeight);
      } catch($e) {
        alert("A problem occurred with your uploaded file.\nReload the page.");
        console.log("takepicture(): problem loading user file.");
      }
    }

    var data = canvas.toDataURL('image/png');
    list = document.getElementById('list');
    list.insertAdjacentHTML("beforeend",
    `<div class="photo-frame" id="photoFrame${imgCounter}" style="border: none" onclick="selectPicture('${photoURL}', '${defaultFilter}', '${imgCounter}')">
    <img class="full-width" src="${data}" id="shot" width="100px" style="border: none">
    </div>`);

    imgCounter++;
  } else {
    clearphoto();
  }
}

function clearphoto() {
  var context = canvas.getContext('2d');
  context.fillStyle = "#F3D83B";
  context.fillRect(0, 0, width, height);
}

function selectPicture(pictureData, filePath, imgCounter) {
  container = document.getElementById('photoFrame'+imgCounter);
  var shots = list.querySelectorAll("div");

  if (container.style.border == 'medium none' || container.style.border == 'none') {
    for (var i = 0; i < shots.length; i++){
      shots[i].style.border = 'none';
    }
    container.style.border  = '5px solid #F3D83B';
  } else {
    container.style.border = 'none';
  }
  updateForm(pictureData, filePath, container);
}

function updateForm(pictureData, filePath, container) {
  var submitButton = document.getElementById('postButton');

  rawPicture.setAttribute('value', "");
  filter.setAttribute('value', "");
  filterSettings.setAttribute('value', "");
  submitButton.setAttribute('disabled', "");

  if (container.style.border != 'medium none') {
    rawPicture.setAttribute('value', pictureData);
    filter.setAttribute('value', filePath);
    filterSettings.setAttribute('value', JSON.stringify(settings[filePath]));
    submitButton.removeAttribute('disabled', "");
  }
}
