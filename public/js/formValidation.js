window.errorColor = '#F44336';

function validateRegistrationForm() {
	var email = document.registration.email;
	var username = document.registration.username;
	var password = document.registration.pwd;
	var pwdRepeat = document.registration.pwdRepeat;

	var emailValid = validateEmail(email);
	var usernameValid = validateUsername(username);
	var pwdValid = validatePassword(password, pwdRepeat);
	if ((emailValid) && (usernameValid) && (pwdValid)) {
		return true;
	}
	return false;
}

function validateForgottenPwdForm() {
	var email = document.forgottenPwd.email;
	return (validateEmail(email));
}

function validateResetPwdForm() {
	var password = document.resetPwd.pwd;
	var pwdRepeat = document.resetPwd.pwdRepeat;
	return (validatePassword(password, pwdRepeat));
}

function validateUsernameForm() {
	var username = document.usernameForm.username;
	return (validateUsername(username));
}

function validateEmailForm() {
	var email = document.emailForm.email;
	return (validateEmail(email));
}

function validateCommentForm(logged) {
	var comment = document.comment.content;
	var commentLength = comment.value.length;
	if (logged == false) {
		var errMessage = "You need to log in to comment this picture.";
	}
	if (commentLength < 1) {
		var errMessage = "Your comment is empty.";
	}
	if (commentLength > 255) {
		var errMessage = "Comments are limited to 255 characters.";
	}
	if (errMessage) {
		errComment.innerHTML = errMessage;
		errComment.style.color = window.errorColor;
		return false;
	}
	return true;
}

function validateEmail(email) {
	var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var errEmailId = document.getElementById("errEmail");
	if (!email.value.match(mailFormat)) {
		errEmailId.innerHTML = "<b> : invalid address</b>";
		errEmailId.style.color = window.errorColor;
		return false;
	}
	errEmailId.innerHTML = "";
	return true;
}

function validateUsername(username) {
	var usernameLength = username.value.length;
	var usernameFormat = "^[a-zA-Z0-9]*$";
	var usernameMatch = username.value.match(usernameFormat);
	var errUsernameId = document.getElementById("errUsername");

	if (usernameLength > 50 || usernameMatch == null) {
		errUsernameId.innerHTML = "<b> : invalid username</b>";
		errUsernameId.style.color = window.errorColor;
		return false;
	}
	errUsernameId.innerHTML = "";
	return true;
}

function validatePassword(password, pwdRepeat) {
	var errPwdId = document.getElementById("errPwd");
	errPwdId.innerHTML = "";
	var errPwdRepeatId = document.getElementById("errPwdRepeat");
	errPwdRepeatId.innerHTML = "";

	if (password.value != pwdRepeat.value) {
		errPwdRepeatId.innerHTML = "<b> : passwords do not match</b>";
		errPwdRepeatId.style.color = window.errorColor;
		return false;
	}

	var pwdLength = password.value.length;
	var pwdStrength = checkPwdStrength(password.value);

	if (pwdLength < 12 || pwdLength > 72 || pwdStrength == false) {
		errPwdId.innerHTML = "<b> : invalid password</b>";
		errPwdId.style.color = window.errorColor;
		return false;
	}
	return true;
}

function checkPwdStrength(password) {
	var pwdRegex = new RegExp("^(?=.*[a-z|A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{12,})");
	if (pwdRegex.test(password)) {
		return true;
	}
	return false;
}
