<?php

require_once __DIR__ . "/model.php";

class PicturesModel extends Model {

	function __construct() {
		parent::__construct();
	}

  public function insertNewPicture($userId, $picture) {
		$query = 'INSERT INTO pictures (UserID, Img) VALUES (:userId, :picture)';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		$stmt->bindParam(':picture', $picture, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to insert new picture in database (code: MP).");
		}
  }

	public function selectAllPictures($page, $maxPictures) {
		$start = $page * $maxPictures;
		$query = "SELECT * FROM pictures ORDER BY CreatedAt DESC LIMIT $maxPictures OFFSET $start";
		$stmt = $this->db->prepare($query);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve pictures from database (code: MP).");
		}
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return ($list);
	}

	public function selectUserPictures($page, $maxPictures, $userId) {
		$start = $page * $maxPictures;
		$query = "SELECT * FROM pictures WHERE UserID=:userId ORDER BY CreatedAt DESC LIMIT $maxPictures OFFSET $start";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve user pictures from database (code: MP).");
		}
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return ($list);
	}

	public function selectPicture($pictureId) {
		$query = "SELECT * FROM pictures WHERE PictureId=:pictureId";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':pictureId', $pictureId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve picture from database (code: MP).");
		}
		$picture = $stmt->fetch(PDO::FETCH_ASSOC);
		return ($picture);
	}

	public function getValueBy($toGet, $column, $value) {
		$query = "SELECT $toGet FROM pictures WHERE $column =:value";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $toGet in DB (code: MP).");
		}
		$output = $stmt->fetchColumn();
		return ($output);
	}

	public function getPictureByUserId($pictureId, $userId) {
		$query = "SELECT * FROM pictures WHERE PictureId=:pictureId AND UserID=:userId";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':pictureId', $pictureId, PDO::PARAM_STR);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $column in DB (code: MP).");
		}
		$output = $stmt->fetchColumn();
		return ($output);
	}

	public function countAllPictures() {
		$query = "SELECT COUNT(PictureId) FROM pictures";
		$stmt = $this->db->prepare($query);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to count pictures in DB (code: MP).");
		}
		$output = $stmt->fetchColumn();
		return ((int)$output);
	}

	public function countUserPictures($userId) {
		$query = "SELECT COUNT(*) FROM pictures WHERE UserID=:userId";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to count user pictures in DB (code: MP).");
		}
		$output = $stmt->fetchColumn();
		return ((int)$output);
	}

	public function deletePicture($pictureId) {
		$query = 'DELETE FROM pictures WHERE PictureId=:pictureId';
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to delete picture in DB (code: MP).");
		}
	}

	public function deleteAll($column, $value) {
		$query = "DELETE FROM pictures WHERE $column=:value";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to delete all $column's pictures in DB (code: MP).");
		}
	}
}
