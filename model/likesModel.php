<?php

require_once __DIR__ . "/model.php";

class LikesModel extends Model {

  public function countLikes($pictureId) {
    $query = 'SELECT COUNT(*) FROM likes WHERE PictureId=:pictureId';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to retrieve likes in DB (code: ML).");
    }
    $output = $stmt->fetchColumn();
    return ((int)$output);
  }

  public function isLiked($userId, $pictureId) {
    $query = 'SELECT COUNT(*) FROM likes WHERE PictureId=:pictureId AND UserId=:userId';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    $stmt->bindValue(':userId', $userId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to retrieve like in DB (code: ML).");
    }
    $output = $stmt->fetchColumn();
    return ((int)$output);
  }

  public function addLike($userId, $pictureId) {
    $query = 'INSERT INTO likes (PictureId, UserID) VALUES (:pictureId, :userId)';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    $stmt->bindValue(':userId', $userId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to insert new like row in DB (code: ML).");
    }
  }

  public function deleteUserLike($userId, $pictureId) {
    $query = 'DELETE FROM likes WHERE PictureId=:pictureId AND UserId=:userId';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    $stmt->bindValue(':userId', $userId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to delete like in DB (code: ML).");
    }
  }

  public function deleteAll($column, $value) {
    $query = "DELETE FROM likes WHERE $column=:value";
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':value', $value, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to delete all $column's likes in DB (code: ML).");
    }
  }
}
