<?php

require_once __DIR__ . "/model.php";

class CommentsModel extends Model {

  public function countComments($pictureId) {
    $query = 'SELECT COUNT(*) FROM comments WHERE PictureId=:pictureId';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to count comments in DB (code: MC).");
    }
    $output = $stmt->fetchColumn();
    return ((int)$output);
  }

  public function selectAllComments($pictureId) {
    $query = "SELECT * FROM comments WHERE pictureId=:pictureId ORDER BY CreatedAt";
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to retrieve comments in DB (code: MC).");
    }
    $list = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return ($list);
  }

  public function insertComment($userId, $pictureId, $content) {
    $query = 'INSERT INTO comments (UserID, PictureId, Content) VALUES (:userId, :pictureId, :content)';
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':userId', $userId, PDO::PARAM_STR);
    $stmt->bindValue(':pictureId', $pictureId, PDO::PARAM_STR);
    $stmt->bindValue(':content', $content, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to insert new comment in DB (code: MC).");
    }
  }

  public function deleteAll($column, $value) {
    $query = "DELETE FROM comments WHERE $column=:value";
    $stmt = $this->db->prepare($query);
    $stmt->bindValue(':value', $value, PDO::PARAM_STR);
    if ($stmt->execute() == FALSE) {
      throw new Exception("Failed to delete all $column's comments in DB (code: MC).");
    }
  }
}
