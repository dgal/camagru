<?php

class Model {

	protected $db;

	function __construct() {
		require __DIR__ . "/../config/database.php";
		try {
			$this->db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOException $err) {
			throw new Exception("Connection to database failed with the following error: " . $err->getMessage() );
		}
	}
}
