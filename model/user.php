<?php

require_once __DIR__ . "/model.php";

class User extends Model {

	function __construct() {
		parent::__construct();
	}

	function getUserInfo($username) {
		$query = 'SELECT * FROM users WHERE Username=:username';
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':username', $username, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve user information in DB (code: MU).");
		}
		$userRow = $stmt->fetch(PDO::FETCH_ASSOC);
		return ($userRow);
	}

	function getValue($column, $value) {
		$query = "SELECT $column FROM users WHERE $column =:value";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $column in DB (code: MU).");
		}
		$output = $stmt->fetchColumn();
		return ($output);
	}

	function getValueBy($toGet, $column, $value) {
		$query = "SELECT $toGet FROM users WHERE $column =:value";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $toGet in DB (code: MU).");
		}
		$output = $stmt->fetchColumn();
		return ($output);
	}

	function getBothValuesBy($toGetFirst, $toGetSecond, $column, $value) {
		$query = "SELECT $toGetFirst, $toGetSecond FROM users WHERE $column =:value";
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $toGetFirst and/or $toGetSecond in DB (code: MU).");
		}
		$output = $stmt->fetch(PDO::FETCH_ASSOC);
		return ($output);
	}

	function getInfo($column, $value) {
		$query = 'SELECT $column FROM users WHERE $column =:value';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve $column in DB (code: MU).");
		}
		$output = $stmt->fetchColumn();
		return ($output);
	}

	function checkIfUserIdExists($userId) {
		$ret = FALSE;
		$query = 'SELECT UserID FROM users WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve user ID in DB (code: MU).");
		}
		$dbUserId = $stmt->fetchColumn();
		if ($dbUserId) {
			$ret = TRUE;
		}
		return ($ret);
	}

	function getUsername($username) {
		$query = 'SELECT Username FROM users WHERE Username=:username';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve username in DB (code: MU).");
		}
		$dbUsername = $stmt->fetchColumn();
		return ($dbUsername);
	}

	function getUsernameByEmail($email) {
		$query = 'SELECT Username FROM users WHERE Email=:email';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve username with email in DB (code: MU).");
		}
		$dbUsername = $stmt->fetchColumn();
		return ($dbUsername);
	}

	function getPassword($column, $user) {
		$query = 'SELECT Password FROM users WHERE ' . $column . '=:user';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':user', $user, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve password in DB (code: MU).");
		}
		$dbPassword = $stmt->fetchColumn();
		return ($dbPassword);
	}

	function getEmail($email) {
		$query = 'SELECT Email FROM users WHERE Email=:email';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve email in DB (code: MU).");
		}
		$dbEmail = $stmt->fetchColumn();
		return ($dbEmail);
	}

	function getEmailByUsername($username) {
		$query = 'SELECT Email FROM users WHERE Username=:username';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve email with username in DB (code: MU).");
		}
		$dbEmail = $stmt->fetchColumn();
		return ($dbEmail);
	}

	function getTmpEmailByUserId($userId) {
		$query = 'SELECT TmpEmail FROM users WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve tmpEmail in DB (code: MU).");
		}
		$dbTmpEmail = $stmt->fetchColumn();
		return ($dbTmpEmail);
	}

	function getActive($username) {
		$query = 'SELECT Active FROM users WHERE Username=:username';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve active status in DB (code: MU).");
		}
		$dbActive = $stmt->fetchColumn();
		return ($dbActive);
	}

	function getToken($column, $user) {
		$query = 'SELECT Token FROM users WHERE '. $column . '=:user';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':user', $user, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve token in DB (code: MU).");
		}
		$dbToken = $stmt->fetchColumn();
		return ($dbToken);
	}

	function getCommentNotif($userId) {
		$query = 'SELECT CommentNotif FROM users WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to retrieve commentNotif status in DB (code: MU).");
		}
		$dbCommentNotif = $stmt->fetchColumn();
		return ($dbCommentNotif);
	}

	function createUser($registrationForm, $pwdHashed, $token) {
		$query = 'INSERT INTO users (Username, Password, Email, Token) VALUES(:username, :password, :email, :token)';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $registrationForm['username'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $pwdHashed, PDO::PARAM_STR);
		$stmt->bindParam(':email', $registrationForm['email'], PDO::PARAM_STR);
		$stmt->bindParam(':token', $token, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to create user in database (code: MU).");
		}
	}

	function updateUserActive($username, $status) {
		$query = 'UPDATE users SET Active=:status WHERE Username=:username';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':status', $status, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user Active status in DB (code: MU).");
		}
	}

	function updateUserToken($token, $column, $user) {
		$query = 'UPDATE users SET Token=:token WHERE ' . $column . '=:user';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':token', $token, PDO::PARAM_STR);
		$stmt->bindParam(':user', $user, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user Token in DB (code: MU).");
		}
	}

	function updateUserPassword($password, $column, $user) {
		$query = 'UPDATE users SET Password=:password WHERE ' . $column . '=:user';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':password', $password, PDO::PARAM_STR);
		$stmt->bindParam(':user', $user, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user Password in DB (code: MU).");
		}
	}

	function updateUsername($username, $userId) {
		$query = 'UPDATE users SET Username=:username WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update username in DB (code: MU).");
		}
	}

	function updateUserTmpEmail($tmpEmail, $userId) {
		$query = 'UPDATE users SET TmpEmail=:tmpEmail WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':tmpEmail', $tmpEmail, PDO::PARAM_STR);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user TmpEmail in DB (code: MU).");
		}
	}

	function updateUserEmail($email, $userId) {
		$query = 'UPDATE users SET Email=:email WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user email in DB (code: MU).");
		}
	}

	function updateUserCommentNotif ($value, $userId) {
		$query = 'UPDATE users SET CommentNotif=:value WHERE UserID=:userId';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':value', $value, PDO::PARAM_STR);
		$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to update user CommentNotif status in DB (code: MU).");
		}
	}

	function deleteUser($column, $user) {
		$query = 'DELETE FROM users WHERE ' . $column . '=:user';
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':user', $user, PDO::PARAM_STR);
		if ($stmt->execute() == FALSE) {
			throw new Exception("Failed to delete user in DB (code: MU).");
		}
	}

}
