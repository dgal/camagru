<?php

session_start();

require __DIR__ . "/controller.php";

class UserController extends Controller {

	private $logAction;
	private $registerAction;
	private $emailAction;
	private $manageAccountAction;
	private $resetPasswordAction;
	private $galleryAction;
	private $commentsAction;
	private $likesAction;

	function __construct() {
		parent::__construct();
		$userDir = 'user';
		$picturesDir = 'pictures';
		$this->logAction = $this->createActionInstance($userDir, 'logAction');
		$this->registerAction = $this->createActionInstance($userDir, 'registerAction');
		$this->manageAccountAction = $this->createActionInstance($userDir, 'manageAccountAction');
		$this->resetPasswordAction = $this->createActionInstance($userDir, 'resetPasswordAction');
		$this->emailAction = $this->createActionInstance('email', 'emailAction');
		$this->galleryAction = $this->createActionInstance($picturesDir, 'galleryAction');
		$this->commentsAction = $this->createActionInstance($picturesDir, 'commentsAction');
		$this->likesAction = $this->createActionInstance($picturesDir, 'likesAction');
	}

	public function loginAction() {
		if (isset($_POST['login']) && isset($_POST['username']) && isset($_POST['password'])) {
			$username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
			$password = htmlspecialchars($_POST['password'], ENT_QUOTES, 'UTF-8');

			$this->error = $this->logAction->checkLoginForm($username, $password);
			if (empty($this->error)) {
				if ($this->logAction->isUserActive($username)) {
					$this->logAction->setLoginSession($username);
				} else {
					try {
						$dbEmail = $this->logAction->getEmailFromDatabase($username);
						$token = $this->logAction->updateUserToken($username);
						$this->emailAction->sendEmail($dbEmail, $username, $token, "validateAccountMessage");
					} catch (Exception $err) {
						die("Error: " . $err->getMessage() );
					}
					$this->error = "Inactive account: we sent you an email to activate your account, check it out !";
				}
			}
		}
		else {
			$this->error = $this->genericError;
		}
		if (!empty($this->error)) {
			$_SESSION['alert'] = $this->error;
			$_SESSION['error'] = TRUE;
		}
		$this->redirect("/");
	}

	public function logoutAction() {
		$this->logAction->logout();
		$this->redirect("/");
	}

	public function registerFormAction() {
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE) {
			$this->notifyError();
		}
		$alert = $this->getAlert();
		Template::render('register', [
			'alert'	=> $alert
		]);
	}

	public function registerSubmitAction() {
		if (isset($_POST['register']) && isset($_POST['email'])
			&& isset($_POST['username']) && isset($_POST['pwd'])
			&& isset($_POST['pwdRepeat'])) {
			$registrationForm = [
				'email' => htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8'),
				'username' => htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8'),
				'pwd' => htmlspecialchars($_POST['pwd'], ENT_QUOTES, 'UTF-8'),
				'pwdRepeat' => htmlspecialchars($_POST['pwdRepeat'], ENT_QUOTES, 'UTF-8'),
			];

			$this->errorBag = $this->registerAction->checkRegistrationForm($registrationForm);
			if (empty($this->errorBag)) {
				$token = $this->registerAction->addUserToDatabase($registrationForm);
				try {
					$this->emailAction->sendEmail($registrationForm['email'], $registrationForm['username'], $token, "validateAccountMessage");
				} catch (Exception $err) {
					die("Error (code: CU): " . $err->getMessage() );
				}
				$_SESSION['alert'] = "Account created ! In order to activate it and be able to log in, please check your emails.";
				$_SESSION['success'] = TRUE;
			}
		} else {
			$this->errorBag['errEmpty'] = $this->genericError;
		}
		if (!empty($this->errorBag)) {
			$_SESSION['error_bag'] = $this->errorBag;
		}
		$this->redirect("/register");
	}

	public function validateAccountAction() {
		if (isset($_GET['username']) && isset($_GET['token']) && isset($_GET['action'])
		&& !empty($_GET['username']) && !empty($_GET['token']) && !empty($_GET['action'])) {
			$username = htmlspecialchars($_GET['username'], ENT_QUOTES, 'UTF-8');
			$token = htmlspecialchars($_GET['token'], ENT_QUOTES, 'UTF-8');
			$action = htmlspecialchars($_GET['action'], ENT_QUOTES, 'UTF-8');

			if ($this->registerAction->checkQueryStringValues($username, $token)) {
				$this->registerAction->updateUserToken($username);
				if ($action == 'confirm' || $action == 'delete') {
					if ($action == 'confirm') {
						$this->registerAction->activateAccount($username);
						$this->notification = "Your registration is complete ! You can now log in.";
					}
					else if ($action == 'delete') {
						$this->deleteAccountAction("Username", $dbUsername);
					}
				$this->notifySuccess($this->notification);
				}
			}
		}
		$this->notifyError();
	}

	public function forgottenPasswordFormAction() {
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE) {
			$this->notifyError();
		}
		$alert = $this->getAlert();
		Template::render('forgottenPassword', [
			'title' => 'Forgotten Password',
			'alert' => $alert
		]);
	}

	public function forgottenPasswordSubmitAction() {
		if (isset($_POST['reset-submit']) && isset($_POST['email'])) {
			$email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');

			$this->error = $this->resetPasswordAction->checkEmail($email);
			if ($this->error != NULL) {
				$_SESSION['alert'] = $this->error;
				$_SESSION['error'] = TRUE;
				$this->redirect("/forgotten-password");
			}

			$username = $this->resetPasswordAction->checkIfUserExists($email);
			$token = $this->resetPasswordAction->updateUserToken($username);
			try {
				$this->emailAction->sendEmail($email, $username, $token, "resetPasswordMessage");
			} catch (Exception $err) {
				die("Error: " . $err->getMessage() );
			}
			$this->notification = "We sent you an email to reset your password, check it out !";
			$this->notifySuccess($this->notification);
		}
		$this->notifyError();
	}

	public function renderPasswordForm($username) {
		$alert = $this->getAlert();
		Template::render('resetPassword', [
			'title'			=> 'Reset Password',
			'username'	=> $username,
			'alert'			=> $alert
		]);
		exit;
	}

	public function resetPasswordFormAction() {
		if (isset($_GET['username']) && isset($_GET['token'])
		&& !empty($_GET['username']) && !empty($_GET['token'])) {
			$username = htmlspecialchars($_GET['username'], ENT_QUOTES, 'UTF-8');
			$token = htmlspecialchars($_GET['token'], ENT_QUOTES, 'UTF-8');

			if ($this->registerAction->checkQueryStringValues($username, $token)) {
				$token = $this->resetPasswordAction->updateUserToken($username);
				$this->renderPasswordForm($username);
			}
		}
		$this->notifyError();
	}

	public function resetPasswordSubmitAction() {
		if (isset($_POST['reset']) && isset ($_POST['username'])
		&& isset($_POST['pwd']) && isset($_POST['pwdRepeat'])) {
			$username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
			$password = htmlspecialchars($_POST['pwd'], ENT_QUOTES, 'UTF-8');
			$pwdRepeat = htmlspecialchars($_POST['pwdRepeat'], ENT_QUOTES, 'UTF-8');

			$this->error = $this->resetPasswordAction->resetPassword($password, $pwdRepeat, $username);
			if (empty($this->error)) {
				$this->notification = 'Your password has been changed, you can now use it to log in !';
				$this->notifySuccess($this->notification);
			}
			else {
				$_SESSION['alert'] = $this->error;
				$_SESSION['error'] = TRUE;
				$this->renderPasswordForm($username);
			}
		}
		$this->notifyError();
	}

	public function displaySettingsAction() {
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE) {
			$status = $this->manageAccountAction->getCheckboxStatus($_SESSION['userId']);
			$alert = $this->getAlert();
			Template::render('settings', [
				'checkbox'	=> $status,
				'alert'			=> $alert
			]);
			exit;
		}
		$this->notifyError();
	}

public function changeSettingsAction() {
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE
		&& isset($_GET['action']) && !empty($_GET['action'])) {
			$userId = $_SESSION['userId'];
			$action = htmlspecialchars($_GET['action'], ENT_QUOTES, 'UTF-8');
			$changeAccountAction = sprintf('changeAccount%s', ucfirst($action));

			if (method_exists($this, $changeAccountAction)) {
				$this->$changeAccountAction($userId);
			}

			if (!empty($this->notification) || !empty($this->error)) {
				if (!empty($this->notification)) {
					$_SESSION['alert'] = $this->notification;
					$_SESSION['success'] = TRUE;
				} else {
					$_SESSION['alert'] = $this->error;
					$_SESSION['error'] = TRUE;

				}
				$this->redirect("/settings");
			}
		}
		$this->notifyError();
	}

	private function changeAccountUsername($userId) {
		$this->error = $this->manageAccountAction->changeUsername($userId);
		if (empty($this->error)) {
			$this->notification = 'Your username has been changed successfully !';
		}
	}

	private function changeAccountEmail($userId) {
		$this->error = $this->manageAccountAction->changeEmail($userId);
		if (empty($this->error)) {
			$email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
			$token = $this->manageAccountAction->updateUserToken($userId);
			try {
				$this->emailAction->sendEmail($email, $userId, $token, "confirmEmailMessage");
			} catch (Exception $err) {
				die("Error: " . $err->getMessage() );
			}
			$this->notification = 'We sent you an email to your new email address, check it out in order to finalise your update !';
		}
	}

	private function changeAccountPassword($userId) {
		$this->error = $this->manageAccountAction->changePassword($userId);
		if (empty($this->error)) {
			$this->notification = 'Your password has been changed successfully !';
		}
	}

	private function changeAccountCommentNotif($userId) {
		$this->manageAccountAction->changeCommentNotif($userId);
		$this->notification = 'Changes saved successfully !';
	}

	private function changeAccountDelete($userId) {
		$this->error = $this->manageAccountAction->deleteAccountRequest($userId);
		if (empty($this->error)) {
			Template::render('notificationPage', [
				'title' 	=> 'Confirmation Request',
				'uri'     => 'delete-account-form',
				'message' => 'Are you sure you want to delete your account ?',
				'choice'	=> 1
			]);
			exit;
		}
	}

	public function confirmEmailAction() {
		if (isset($_GET['uid']) && isset($_GET['token'])
		&& !empty($_GET['uid']) && !empty($_GET['token'])) {
			$userId = htmlspecialchars($_GET['uid'], ENT_QUOTES, 'UTF-8');
			$token = htmlspecialchars($_GET['token'], ENT_QUOTES, 'UTF-8');

			if ($this->manageAccountAction->checkQueryStringValues($userId, $token)) {
				$this->manageAccountAction->updateUserToken($userId);
				$this->manageAccountAction->updateUserEmail($userId);
				$this->notification= 'Your email has been updated successfully !';
				$this->notifySuccess($this->notification);
			}
		}
		$this->notifyError();
	}

	public function deleteAccountFormAction() {
		if (isset($_POST['action']) && !empty($_POST['action'])) {
			if ($_POST['action'] == 'yes') {
				$this->deleteAccountAction("UserID", $_SESSION['userId']);
			}
			else if ($_POST['action'] == 'cancel') {
				$this->redirect("/settings");
			}
		}
		$this->notifyError();
	}

	public function deleteAccountAction($column, $user) {
		$this->galleryAction->deleteAllPictures($column, $user);
		$this->commentsAction->deleteComments($column, $user);
		$this->likesAction->deleteLikes($column, $user);
		$this->manageAccountAction->deleteAccount($column, $user);
		$this->logAction->clearSession();
		$this->notification = "Your account has been deleted successfully.";
		$this->notifySuccess($this->notification);
	}

}
