<?php

session_start();

require __DIR__ . "/controller.php";

class HomeController extends Controller {

	private $picturesController;

	function __construct() {
		if (!class_exists('PicturesController')) {
			require __DIR__ . "/pictures.php";
		}
		$this->picturesController = new PicturesController;
	}

	public function indexAction() {
		$this->picturesController->displayGalleryAction();
	}
}
