<?php

session_start();

class Controller {

  protected $genericError = "An error has occurred. Please try again later.";
  protected $notification;
  protected $errorBag = array();
  protected $error = NULL;

  function __construct() {
    require_once __DIR__ . "/../helper/Template.php";
  }

  protected function createActionInstance($folder, $class) {
    if (!class_exists($class)) {
      require __DIR__ . "/../actions/$folder/$class.php";
    }
    $class = ucfirst($class);
    return(new $class);
  }

  protected function notifyError() {
    Template::render('notificationPage', [
      'title' => 'Error request',
      'message' => $this->genericError
    ]);
    exit;
  }

  protected function notifySuccess($notification) {
    Template::render('notificationPage', [
      'title' => 'Confirmation',
      'message' => $notification
    ]);
    exit;
  }

  protected function getAlert() {
    if (isset($_SESSION['alert']) && !empty($_SESSION['alert'])) {
      if (isset($_SESSION['success']) && $_SESSION['success'] === TRUE) {
        $alert = "success";
      } elseif (isset($_SESSION['error']) && $_SESSION['error'] === TRUE) {
        $alert = "errors";
      }
      return($alert);
    }
  }

  protected function redirect($uri) {
    header("Location: $uri");
    exit;
  }
}
