<?php

session_start();

require_once __DIR__ . "/controller.php";

class PicturesController extends Controller {

  private $montageAction;
  private $galleryAction;
  private $likesAction;
  private $commentsAction;
  private $emailAction;

  function __construct() {
    parent::__construct();
    $folder = 'pictures';
    $this->montageAction = $this->createActionInstance($folder, 'montageAction');
    $this->galleryAction = $this->createActionInstance($folder, 'galleryAction');
    $this->likesAction = $this->createActionInstance($folder, 'likesAction');
    $this->commentsAction = $this->createActionInstance($folder, 'commentsAction');
    $this->emailAction = $this->createActionInstance('email', 'emailAction');
  }

  public function displayCameraAction() {
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE) {
      $alert = $this->getAlert();
      Template::render('camera', [
        'alert' => $alert
      ]);
      exit;
    }
    $this->notifyError();
  }

  public function createMontageAction() {
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == TRUE) {
      $this->error = $this->montageAction->checkPostEntries();
      if (empty($this->error)) {
        $this->error = $this->montageAction->createMontage();
      }
      if (empty($this->error)) {
        $this->montageAction->addPictureToDatabase();
        $_SESSION['alert'] = "Your picture has been uploaded successfully !";
        $_SESSION['success'] = TRUE;
      }
      if (!empty($this->error)) {
        $_SESSION['alert'] = $this->error;
        $_SESSION['error'] = TRUE;
      }
      $this->redirect('/camera');
    }
    $this->notifyError();
  }

  public function displayGalleryAction() {
    $user = FALSE;
    $page = $this->galleryAction->getPageNumber();
    $pictures = $this->galleryAction->getPictures($page, $user);
    foreach ($pictures as &$picture) {
      $picture['username'] = $this->galleryAction->getUsername($picture['UserID']);
      $picture['isliked'] = $this->likesAction->isPictureLiked($picture['PictureId']);
      $picture['likes'] = $this->likesAction->countLikes($picture['PictureId']);
      $picture['comments'] = $this->commentsAction->countComments($picture['PictureId']);
    }

    $pageMax = $this->galleryAction->getPageMax($user);
    $alert = $this->getAlert();
    Template::render('index', [
      'pictures'  => $pictures,
      'page'      => $page,
      'pageMax'   => $pageMax,
      'alert'     => $alert
    ]);
    exit;
  }

  public function addLikeAction() {
    if (isset($_GET['id']) && is_numeric($_GET['id'])) {
      $id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
    }
    if (isset ($_SESSION['loggedin']) && $_SESSION['loggedin'] === TRUE ) {
      $this->likesAction->addLike($id);
    }
    $location = htmlspecialchars($_GET['from'], ENT_QUOTES, 'UTF-8');
    if ($location == '/picture-focus') {
      $what = 'id';
    } else {
      $what = 'page';
      $id = htmlspecialchars($_GET['page'], ENT_QUOTES, 'UTF-8');
    }
    $this->redirect("$location?$what=$id");
  }

  public function displayUserPicturesAction() {
    if (!isset($_SESSION['userId']) || empty($_SESSION['userId'])) {
      $_SESSION['alert'] = "You need to log in to see your pictures.";
      $_SESSION['error'] = TRUE;
      $this->redirect('/');
    }
    $page = $this->galleryAction->getPageNumber();
    $userPictures = $this->galleryAction->getPictures($page, $_SESSION['userId']);
    $pageMax = $this->galleryAction->getPageMax($_SESSION['userId']);
    foreach ($userPictures as &$userPicture) {
      $userPicture['isliked'] = $this->likesAction->isPictureLiked($userPicture['PictureId']);
      $userPicture['likes'] = $this->likesAction->countLikes($userPicture['PictureId']);
      $userPicture['comments'] = $this->commentsAction->countComments($userPicture['PictureId']);
    }
    $alert = $this->getAlert();
    Template::render('myPictures', [
      'title'         => 'My Pictures',
      'userPictures'  => $userPictures,
      'page'          => $page,
      'pageMax'       => $pageMax,
      'alert'         => $alert
    ]);
  }

  public function deletePictureRequestAction() {
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === TRUE
      && isset($_GET['id']) && is_numeric($_GET['id'])) {
        $pictureId = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
        $userId = $_SESSION['userId'];
        if ($this->galleryAction->checkPictureRights($pictureId, $userId)) {
          Template::render('notificationPage', [
            'title'   => 'Confirmation Request',
            'uri'     => "delete-picture-form?id=$pictureId",
            'message' => 'Are you sure you want to delete this picture ?',
            'choice'  => 1
          ]);
          exit;
        }
    }
    $this->notifyError();
  }

  public function deletePictureFormAction() {
    if (isset($_POST['action']) && !empty($_POST['action'])
      && isset($_GET['id']) && is_numeric($_GET['id'])) {
      if ($_POST['action'] === 'yes') {
        $pictureId = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
        $this->galleryAction->deletePicture($pictureId);
        $this->likesAction->deleteLikes('PictureId', $pictureId);
        $this->commentsAction->deleteComments('PictureId', $pictureId);
        $_SESSION['alert'] = 'Your picture has been deleted successfully !';
        $_SESSION['success'] = TRUE;
      }
      $this->redirect("/my-pictures");
    }
    $this->notifyError();
  }

  public function displayPictureAction() {
    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
      $this->redirect('/');
    }
    $logged = isset($_SESSION['loggedin']) ? TRUE : FALSE;
    $page = $this->galleryAction->getPageNumber();
    $pictureId = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
    $picture = $this->galleryAction->getPicture($pictureId);
    if (empty($picture)) {
      $this->notifyError();
    }
    $picture['username'] = $this->galleryAction->getUsername($picture['UserID']);
    $picture['isliked'] = $this->likesAction->isPictureLiked($pictureId);
    $picture['likes'] = $this->likesAction->countLikes($pictureId);
    $picture['commentsNb'] = $this->commentsAction->countComments($pictureId);
    $comments = $this->commentsAction->getComments($pictureId);
    $alert = $this->getAlert();
    Template::render('pictureFocus', [
      'title'     => 'Focus',
      'log'       => $logged,
      'picture'   => $picture,
      'comments'  => $comments,
      'page'      => $page,
      'alert'     => $alert
    ]);
  }

  public function addCommentFormAction() {
    if (!isset($_GET['id']) || !is_numeric($_GET['id']) || !isset($_POST['content'])) {
      $this->redirect('/');
    }
    $pictureId = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
    $comment = htmlspecialchars($_POST['content'], ENT_QUOTES, 'UTF-8');
    $this->error = $this->commentsAction->validateComment($comment);
    if (empty($this->error)) {
      $this->commentsAction->addComment($_SESSION['userId'], $pictureId, $comment);
      if ($this->commentsAction->hasMailOption($pictureId)) {
        $infoBag = $this->commentsAction->prepareEmailArgs($pictureId);
        try {
          $this->emailAction->sendEmail($infoBag['Email'], $infoBag['Username'], $pictureId, "notifyComment");
        } catch (Exception $err) {
          die("Error (code:CU): " . $err->getMessage() );
        }
      }
    }
    if (!empty($this->error)) {
      $_SESSION['alert'] = $this->error;
      $_SESSION['error'] = TRUE;
    }
    $this->redirect("/picture-focus?id=$pictureId");
  }

}
