<?php

class MailTemplate {

	private $dirPath = __DIR__ . "/../view/mails/";

	function validateAccountMessage($username, $token) {
		$confirmationLink = 'https://www.camagru.com/validate-account?username=' . $username . '&token=' . $token;
		ob_start();
		include($this->dirPath . "validateAccount.html.php");
		return ob_get_clean();
	}

	function resetPasswordMessage($username, $token) {
		$resetLink = 'https://www.camagru.com/reset-password?action=reset&username=' . $username . '&token=' . $token;
		ob_start();
		include($this->dirPath . "resetPassword.html.php");
		return ob_get_clean();
	}

	function confirmEmailMessage($userId, $token) {
		$confirmationLink = 'https://www.camagru.com/confirm-email?uid=' . $userId . '&token=' . $token;
		ob_start();
		include($this->dirPath . "confirmEmail.html.php");
		return ob_get_clean();
	}

	function notifyComment($username, $pictureId) {
		$pictureLink = 'https://www.camagru.com/picture-focus?id=' . $pictureId;
		ob_start();
		include($this->dirPath . "commentNotification.html.php");
		return ob_get_clean();
	}
}
