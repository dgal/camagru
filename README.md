# CAMAGRU PROJECT

## Pitch 

42 school web project.  
Create a light version of an insta-like based website. It will allow you to make basic photo and video editing using your webcam and predefined images.

## Authorized languages

- SERVER : PHP
- CLIENT: HTML, CSS, JavaScript (only with browser natives API)

## Authorized frameworks

- SERVER: None
- CLIENT: CSS Frameworks tolerated, unless it adds forbidden JavaScript

## Architecture

- MVC
- File architecture inspired by [Porto][0].

## New skills acquired

**Languages:**
- PHP, HTML, CSS, JavaScript, SQL.

**Use of Imagick extension**

**Object-Oriented Programming (OOP)**

**MVC architecture:**
- concept, 
- advantages, 
- implementation of a template engine (without frameworks),
- implementation of a router.

**Working on clean architecture and clean code**

**Working on clean commits**

Resource: [Conventional commits][1]

## Screenshots

![gallery preview][2]

![picture preview][3]

![camera preview][4]

![registration form preview][5]

[0]: https://github.com/Mahmoudz/Porto
[1]: https://www.conventionalcommits.org/en/v1.0.0/#summary
[2]: /public/images/screenshots/gallery-logged.png
[3]: /public/images/screenshots/picture-focus.png
[4]: /public/images/screenshots/camera.png
[5]: /public/images/screenshots/create-account.png
